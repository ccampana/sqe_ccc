#include "vars_arrays.h"
#include <math.h>
#include "memory.h"
#include "ewald.h"

double energy_bond_elastic(int);
double energy_atomic_elastic(int);
double energy_total(void);
