#define NDIM 3
#define PI (4*atan(1.0))
#define small 1.e-15
/////////////////////////////////////////////////////////////////////////
// SplitQs variables
extern int n_splitQs, n_atoms, k_order_max, k_order_2_max, n_order_max[NDIM];

extern int *multiplicity_SplitQs, **atom_indices_SplitQs, **indices_SplitQs, \
           charge_flag;

extern double R_cutoff;

extern double damping_factor, error_tol, time_step, Vol_cell, alpha_Ewald;

extern double *SplitQs, *vel_SplitQs, *mass_SplitQs, *Kappa_SplitQs, \
        **atom_pos, *atom_Psi, *atom_Kappa, *atom_rad, *Psi_SplitQs;

extern double *atom_Q, **recp_term_elect, **real_term_elect, **real_lat_vec, \
       **recp_lat_vec;

extern int ktot;

extern double **sum_real, **sum_recp, **kvec;

////////////////////////////////////////////////////////////////////////
// CG-variables
extern double ecurrent, *hcg, *fcg, *x0cg, *xcg;

////////////////////////////////////////////////////////////////////////
// Simulated annealing variables
extern int n_anneal_moves, n_params;

extern double Temp_max, *anneal_params;

extern double *atom_Q_anneal_ref;
