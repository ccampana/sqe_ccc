#include "random.h"
#include "memory.h"
#include "vars_arrays.h"
#include <math.h>
#include "compute_charges.h"
#include "minimize_energy.h"

void sim_annealing(void);
void evolve_temperature(int,int&,double&);
int test_move_quality(int,double&);
double compute_chi_square(void);
void transfer_params(void);
void print_anneal_data(int,double);
void load_ref_charges(void);
