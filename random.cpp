//////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include "math.h"
#include "random.h"
#include "memory.h"
///////////////////////////////////////////////////////////////////////
/* Local variables */
int save;
double second;
double *u;
int i97,j97;
double c,cd,cm;
////////////////////////////////////////////////////////////////////////
/* Function definitions */
////////////////////////////////////////////////////////////////////////
/* Marsaglia RNG initializer */
void RanMars(int seed) {

  int ij,kl,i,j,k,l,ii,jj,m;
  double s,t;

  if (seed <= 0 || seed > 900000000)
    printf("Invalid seed for Marsaglia random # generator");

  save = 0;
  u = create_1d_double_array(97+1,"Marsaglia");

  ij = (seed-1)/30082;
  kl = (seed-1) - 30082*ij;
  i = (ij/177) % 177 + 2;
  j = ij %177 + 2;
  k = (kl/169) % 178 + 1;
  l = kl % 169;
  for (ii = 1; ii <= 97; ii++) {
    s = 0.0;
    t = 0.5;
    for (jj = 1; jj <= 24; jj++) {
      m = ((i*j) % 179)*k % 179;
      i = j;
      j = k;
      k = m;
      l = (53*l+1) % 169;
      if ((l*m) % 64 >= 32) s = s + t;
      t = 0.5*t;
    }
    u[ii] = s;
  }
  c = 362436.0 / 16777216.0;
  cd = 7654321.0 / 16777216.0;
  cm = 16777213.0 / 16777216.0;
  i97 = 97;
  j97 = 33;
  uniform();

}
////////////////////////////////////////////////////////////////////////
/* Uniform RNG */
double uniform() {

  double uni = u[i97] - u[j97];
  if (uni < 0.0) uni += 1.0;
  u[i97] = uni;
  i97--;
  if (i97 == 0) i97 = 97;
  j97--;
  if (j97 == 0) j97 = 97;
  c -= cd;
  if (c < 0.0) c += cm;
  uni -= c;
  if (uni < 0.0) uni += 1.0;
  return uni;

}

