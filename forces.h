#include "vars_arrays.h"
#include <math.h>
#include <stdio.h>

int get_index(int,int);
double force_Langevin(int);
double force_bond_elastic(int);
double force_atomic_elastic(int,int);
double force_self_term(int,int);
double force_electrostatics(int,int);

