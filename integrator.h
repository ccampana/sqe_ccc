#include "compute_charges.h"
#include "forces.h"
#include "memory.h"
#include <stdio.h>
#include "energies.h"

void predict_SplitQs(void);
void monitor_Qs(int);
void md_on_SplitQs(void);
