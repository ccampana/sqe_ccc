#include"energies.h"

/////////////////////////////////////////////////////////////////////////////
// Computing elastic energy from the bond hardness
double energy_bond_elastic(int i_SplitQ) {

       double energy_bond;

       energy_bond = (0.5*Kappa_SplitQs[i_SplitQ]*pow(SplitQs[i_SplitQ],2) +  \
       Psi_SplitQs[i_SplitQ]*SplitQs[i_SplitQ])/multiplicity_SplitQs[i_SplitQ];

       return energy_bond;

}
////////////////////////////////////////////////////////////////////////////
// Computing elastic energy from atomic contribution
double energy_atomic_elastic(int i_atom) {

       double energy_atomic;

       energy_atomic = 0.5*atom_Kappa[i_atom]*pow(atom_Q[i_atom],2) + \
       atom_Psi[i_atom]*atom_Q[i_atom];

       return energy_atomic;

}
//////////////////////////////////////////////////////////////////////////////
double energy_total(void) {

       int i_SplitQ, i_atom;
       double ebond, eatomic, ereal, erecp, eself, etotal;

       ereal = compute_real_ener();
       erecp = compute_recp_ener();
       eself = compute_self_term_ener();
       etotal = ereal + erecp + eself;

       for(i_atom=0;i_atom<n_atoms;i_atom++){
          eatomic = energy_atomic_elastic(i_atom);
          etotal += eatomic;
       }

       for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
          ebond = energy_bond_elastic(i_SplitQ);
          etotal += ebond;
       }

       return etotal;

}



