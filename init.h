#include "vars_arrays.h"
#include "memory.h"
#include <math.h>
#include "ewald.h"

void initialization(int,double**,int**,double**,double**,double*);
double matrix_inverse(double**,double**);
void set_lattice_vectors(double**);
void clear_memory(void);
