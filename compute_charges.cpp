#include "compute_charges.h"
///////////////////////////////////////////////////////////////////
// Computing charges on atoms
void compute_atomic_Qs(void) {

     int i_atom, j_atom, i_SplitQ;

     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        atom_Q[i_atom] = 0.0;
        for(j_atom=0;j_atom<n_atoms;j_atom++) {
           i_SplitQ = indices_SplitQs[i_atom][j_atom];
           if(j_atom < i_atom) {
             atom_Q[i_atom] -= SplitQs[i_SplitQ];
           } else if(j_atom > i_atom) {
             atom_Q[i_atom] += SplitQs[i_SplitQ];
           }
        }
     }

}


