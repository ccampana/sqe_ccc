#include "vars_arrays.h"
#include "forces.h"
#include "memory.h"
#include "compute_charges.h"
#include "energies.h"
#include "integrator.h"

#define MIN(A,B) ((A) < (B) ? (A) : (B))
#define MAX(A,B) ((A) > (B) ? (A) : (B))

double compute_gradient(double*,double*);
int min_energy_CG(void);
int line_search_backtrack(double,double&);
double alpha_step(double);

