#include "annealing.h"
///////////////////////////////////////////////////////////////////////////
// Simulating annealing routine to optimize SQE parameters
void sim_annealing(void) {

     int i_param, i_move, peak, *fix_flag, status, SEED;

     double Temp, *delta_params, urn, param_step, dinc, \
     *accept_ratio, E_old, E_start, E_curr, E_trial, boltz_prob, \
     target_ratio, delta_param_perc, E_conv_tol;

     peak = 0;
     Temp_max = 1.0;
     n_anneal_moves = 500;
     dinc = 0.01;
     delta_param_perc = 0.02;
     target_ratio = 0.5;
     SEED = 6789514;
     E_conv_tol = 1e-6;
     // Estimating the # of params for the annealing
     n_params = 5;

     anneal_params = create_1d_double_array(n_params,"parameters");
     delta_params = create_1d_double_array(n_params,"parameters increments");
     accept_ratio = create_1d_double_array(n_params,"acceptance ratio");
     fix_flag = create_1d_int_array(n_params,"stop flag");

     // Initialization
     RanMars(SEED);
     for(i_param=0;i_param<n_params;i_param++) {
        fix_flag[i_param] = 0;
        anneal_params[i_param] = 0.0;
        accept_ratio[i_param] = target_ratio;
        delta_params[i_param] = delta_param_perc;
     }
     anneal_params[0] = 1.0; // 2.00086      1.066000;
     anneal_params[1] = -7.10; //0.104476      0.7995914;
     anneal_params[2] = 0.31; //0.314714       0.44675767;
     anneal_params[3] = 78.16; //0.168749        -0.5392226;
     anneal_params[4] = 11.36; //0.363516       0.2770736;

     E_start = compute_chi_square();
     E_old = E_start;
     E_trial = E_start;
     Temp = Temp_max;

     for(i_move=0;i_move<n_anneal_moves;i_move++) {
        // Temperature evolution
        evolve_temperature(i_move,peak,Temp);
        for(i_param=0;i_param<n_params;i_param++) {
           if(fix_flag[i_param] == 0) {
             urn = uniform();
             param_step = 2*(urn-0.5)*delta_params[i_param];
             anneal_params[i_param] += param_step;
             status = test_move_quality(i_param,E_trial);
             // Good move. Proceed to cost function calculation
             if(status == 1) {
               E_trial = compute_chi_square();  
               // Exiting if convergence totlerance achieved
               if(E_trial < E_conv_tol) {
                 print_anneal_data(i_move,E_trial);
                 printf("E_trial end is %f\n",E_trial);
                 exit(EXIT_SUCCESS);
               }
             }
             // The Boltzman test
             if(E_trial - E_old <= 0.0) {
               boltz_prob = 1.0;
             } else if(((E_trial - E_old)/Temp > 10.0) || \
               (E_trial == E_old)) {
               boltz_prob = 0.0;
             } else {
               boltz_prob = exp(-(E_trial - E_old)/Temp);
               urn = uniform();
             }
             accept_ratio[i_param] *= (1.0 - dinc);
             if(urn < boltz_prob) {
               E_old = E_trial;
               accept_ratio[i_param] += dinc;
               print_anneal_data(i_move,E_old);
             } else {
               anneal_params[i_param] -= param_step;
             }
             delta_params[i_param] *= \
             (accept_ratio[i_param]-target_ratio) + 1.0;
             if(accept_ratio[i_param] <= small) fix_flag[i_param] = 1;
           }
        }

     }
     
     destroy_1d_double_array(anneal_params);
     destroy_1d_double_array(delta_params);
     destroy_1d_double_array(accept_ratio);
     destroy_1d_int_array(fix_flag);

}
///////////////////////////////////////////////////////////////////////
// Evolving annealing temperature
void evolve_temperature(int i_move,int& peak, double& Temp) {

    int quarter_moves;

    double times, ratio;

    times = 100.0;
    ratio = 0.9;
    quarter_moves = n_anneal_moves/4;
    
    if(i_move <= quarter_moves) {
      Temp *= ratio;
    } else if ((i_move > quarter_moves) && (Temp <= Temp_max/times) && \
      (peak == 0) ) {
      Temp /= ratio;
      if(Temp >= Temp_max/times) peak = 1;
    } else {
      Temp *= ratio;
    }

}
//////////////////////////////////////////////////////////////////////
// Testing move quality. Here is where constaints could be added to
// the parameter space exploration
int test_move_quality(int i_param,double& Energy) {

    int answ;

    answ = 1;

    // Preventing bonds hardness from becoming negative
    if(i_param == 0) {
      if(anneal_params[i_param] < -small) {
        Energy = 1.0e20;
        answ = 0;
      }
    }
    return answ;

}
///////////////////////////////////////////////////////////////////////
// Computing the cost function
double compute_chi_square(void) {

       int i_atom, iflag;

       double sum_up, sum_down, Chi_square;

       sum_up = 0.0;
       sum_down = 0.0; 

       // Transfering the parameters
       transfer_params();
       
       // Computing charges using the CG-method
       iflag = min_energy_CG();
       printf(" iflag is = %d\n",iflag);        
       //if((iflag != 1) && (iflag != 2)) exit(EXIT_FAILURE);
       for(i_atom=0;i_atom<n_atoms;i_atom++) {
          sum_up += pow((atom_Q[i_atom]-atom_Q_anneal_ref[i_atom]),2);
          sum_down += pow(atom_Q_anneal_ref[i_atom],2);
       }
       Chi_square = sum_up/sum_down;

       return Chi_square;
     
}
//////////////////////////////////////////////////////////////////////////
// Transfering parameters between the simmulated annealing and the splitq
// section of the code
void transfer_params(void) {

     int i_param;
    
     for(i_param=0;i_param<n_splitQs;i_param++) { 
         Kappa_SplitQs[i_param] = anneal_params[0];
         Psi_SplitQs[i_param] = 0.0; //anneal_params[1] - anneal_params[3];
         SplitQs[i_param] = 0.0; 
     }

/*

     atom_Psi[0] =  anneal_params[1]; 
     atom_Kappa[0] =  anneal_params[2];
     atom_Psi[1] = anneal_params[3];
     atom_Kappa[1] = anneal_params[4];


     atom_Kappa[0] =  atom_Kappa[1] = anneal_params[1];
     atom_Kappa[2] =  atom_Kappa[3] = anneal_params[2];
     atom_Psi[0] = atom_Psi[1] = anneal_params[3];
     atom_Psi[2] = atom_Psi[3] = anneal_params[4];

*/

     for(i_param=0;i_param<n_atoms;i_param++) {
        if(i_param<n_atoms/2) {
          atom_Psi[i_param] = anneal_params[1];
          atom_Kappa[i_param] = anneal_params[2];
        } else { 
          atom_Psi[i_param] = anneal_params[3];
          atom_Kappa[i_param] = anneal_params[4];
        }
     }

 
}
//////////////////////////////////////////////////////////////////////////
// Printing annealing data
void print_anneal_data(int i_move, double E_cost) {

     int i_atom, i_param;

     FILE *file_output_anneal_Qs, *file_output_anneal_params;

     file_output_anneal_Qs = fopen("anneal_Qs.dat","a");
     file_output_anneal_params = fopen("anneal_params.dat","a");
     fprintf(file_output_anneal_Qs,"%d ",i_move);
     fprintf(file_output_anneal_params,"%d ",i_move);
     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        fprintf(file_output_anneal_Qs,"%f ",atom_Q[i_atom]);
     }
     for(i_param=0;i_param<n_params;i_param++) {
        fprintf(file_output_anneal_params,"%f ",anneal_params[i_param]);
     }        
     fprintf(file_output_anneal_Qs,"%f\n ",E_cost);
     fprintf(file_output_anneal_params,"%f\n ",E_cost);
     fclose(file_output_anneal_Qs);
     fclose(file_output_anneal_params);

}
///////////////////////////////////////////////////////////////////////////
// Loading reference charges 
void load_ref_charges(void) {

     int i_atom;

     compute_atomic_Qs();
     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        atom_Q_anneal_ref[i_atom] = atom_Q[i_atom];
     }

}
