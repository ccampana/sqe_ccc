#include "vars_arrays.h"
#include <math.h>
#include "forces.h"
#include <complex> 
#include "memory.h"

void compute_recp_term(void);
void compute_real_term(void);
void compute_recp_term_ener(void);
double compute_recp_ener(void);
void compute_real_term_ener(void);
double  compute_real_ener(void);
double compute_self_term_ener(void);
