#include "minimize_energy.h"
////////////////////////////////////////////////////////////
//  This is a CG implementation of the SQE energy based on the
//  min_cg.cpp lammps implementation of the conjugate-gradient 
//  algorithm
enum{MAXITER,ETOL,FTOL,DOWNHILL,ZEROFORCE,ZEROALPHA};

//////////////////////////////////////////////////////////////
// Minimizing the energy via CG
int min_energy_CG(void) {

    int i_SplitQ, intflag;
    double dotall[2], beta;
    double EPS_ENERGY, etol, ftol, *gcg, gg, eprevious, alpha;

    etol = 1.0e-12;
    ftol = 1.0e-5;
    EPS_ENERGY = 1.0e-8;

    // Creating arrays involved
    gcg = create_1d_double_array(n_splitQs,"");
    fcg = create_1d_double_array(n_splitQs,"");
    hcg = create_1d_double_array(n_splitQs,"");
    xcg = create_1d_double_array(n_splitQs,"");
    x0cg = create_1d_double_array(n_splitQs,"");
 
    for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
       xcg[i_SplitQ] = SplitQs[i_SplitQ];
    }

    // Computing the energy and gradient
    ecurrent = compute_gradient(xcg,fcg);

    // Initializing the CG arrays
    for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
       hcg[i_SplitQ] = gcg[i_SplitQ] = fcg[i_SplitQ];
    }

    gg = 0.0;
    for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
       gg += pow(fcg[i_SplitQ],2);
    }

    // CG loop
    int niter, niter_max;

     niter = 1;
     niter_max = 10000;
     while(niter <= niter_max) {

          monitor_Qs(niter);
          //printf("Iteration %d done!\n",niter);
          eprevious = ecurrent;
          intflag = line_search_backtrack(ecurrent,alpha);

          // energy tolerance criterion
          if (fabs(ecurrent-eprevious) < \
            etol*0.5*(fabs(ecurrent) + fabs(eprevious) + EPS_ENERGY)) {
            printf("Done in %d iterations after energy convergence\n",niter);
            return ETOL;
          }

          // force tolerance criterion
          dotall[0] = dotall[1] = 0.0;
          for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
             dotall[0] += pow(fcg[i_SplitQ],2);
             dotall[1] += fcg[i_SplitQ]*gcg[i_SplitQ];
          }

          if (dotall[0] < ftol*ftol) {
             printf("Done in %d iterations after force convergence\n",niter);
             return FTOL;
          }

          // Update new search direction using Polak-Ribieri
          beta = MAX(0.0,(dotall[0] - dotall[1])/gg);
          gg = dotall[0];

          for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
             gcg[i_SplitQ] = fcg[i_SplitQ];
             hcg[i_SplitQ] = gcg[i_SplitQ] + beta*hcg[i_SplitQ];
          }

          // Restart CG if new search direction h is not
          // downhill
          dotall[0] = 0.0;
          for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
             dotall[0] += gcg[i_SplitQ]*hcg[i_SplitQ];
          }
          if (dotall[0] <= 0.0) {
             for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
                hcg[i_SplitQ] = gcg[i_SplitQ];
             }
          }
          niter++;
     }

     destroy_1d_double_array(gcg);
     destroy_1d_double_array(fcg);
     destroy_1d_double_array(hcg);
     destroy_1d_double_array(x0cg);
     destroy_1d_double_array(xcg);

     return MAXITER;

}
//////////////////////////////////////////////////////////////
// Line search algorithm
int line_search_backtrack(double eoriginal, double &alpha){

    int i_SplitQ;
    double ALPHA_MAX, ALPHA_REDUCE, BACKTRACK_SLOPE, EMACH, \
    fdothall, dmax, hmaxall, de, de_ideal;

    ALPHA_MAX = 1.0;
    ALPHA_REDUCE = 0.5;
    BACKTRACK_SLOPE = 0.4;
    EMACH = 1.0e-5;
    dmax = 0.01;

    fdothall = 0.0;
    hmaxall = 0.0;
    for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
       fdothall += fcg[i_SplitQ]*hcg[i_SplitQ];
       hmaxall = MAX(hmaxall,fabs(hcg[i_SplitQ]));
    }

    alpha = MIN(ALPHA_MAX,dmax/hmaxall);
    if (fdothall <= 0.0) return DOWNHILL;
    if (hmaxall == 0.0) return ZEROFORCE;

    for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
       x0cg[i_SplitQ] = xcg[i_SplitQ];
    }

    while (1) {
          ecurrent = alpha_step(alpha);
          de_ideal = -BACKTRACK_SLOPE*alpha*fdothall;
          de = ecurrent - eoriginal;
          if (de <= de_ideal) {
             return 0;
          }

          alpha *= ALPHA_REDUCE;
          if (alpha <= 0.0 || de_ideal >= -EMACH) {
             ecurrent = alpha_step(0.0);
             return ZEROALPHA;
           }
    }

}
//////////////////////////////////////////////////////////
// Alpha calculation within the line search method
double alpha_step(double alpha) {

       int i_SplitQ;
       double energy;

       // Storing the x vectors
       for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
          xcg[i_SplitQ] = x0cg[i_SplitQ];
       }
     
       if (alpha > 0.0) {
          for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
             xcg[i_SplitQ] += alpha*hcg[i_SplitQ];
          }
       }
       energy = compute_gradient(xcg,fcg);

       return energy;

}
///////////////////////////////////////////////////////////
// Energy and force calculation via SQE
double compute_gradient(double* pos, double* force) {

       int i_SplitQ, i_atom, j_atom;

       double bond_force, atomic_force, elect_force, energy;

       for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {  
          SplitQs[i_SplitQ] = pos[i_SplitQ];
       }

       compute_atomic_Qs(); 
       for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
          i_atom = atom_indices_SplitQs[i_SplitQ][0];
          j_atom = atom_indices_SplitQs[i_SplitQ][1];
       
          bond_force = force_bond_elastic(i_SplitQ);
          atomic_force = force_atomic_elastic(i_atom,j_atom);
          elect_force = force_electrostatics(i_atom,j_atom);
          force[i_SplitQ] = (bond_force + atomic_force + elect_force);
       }
       energy = energy_total();
       return energy;

}
