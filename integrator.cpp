#include "integrator.h"
////////////////////////////////////////////////////////////////////////////
// Computing split charges using velocity verlet
void predict_SplitQs(void) {

     int i_SplitQ, i_atom, j_atom;
     double bond_force, damping_force, atomic_force, \
            elect_force, force_old, force_new;

     compute_atomic_Qs(); 

     for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
        i_atom = atom_indices_SplitQs[i_SplitQ][0];
        j_atom = atom_indices_SplitQs[i_SplitQ][1];
        // Force calculation
        bond_force = force_bond_elastic(i_SplitQ);
        damping_force = force_Langevin(i_SplitQ);
        atomic_force = force_atomic_elastic(i_atom,j_atom);
        elect_force = force_electrostatics(i_atom,j_atom);
        force_old = bond_force + atomic_force + damping_force + \
        elect_force;

        // Coord and vel propagation
        SplitQs[i_SplitQ] += vel_SplitQs[i_SplitQ]*time_step + \
        force_old*pow(time_step,2)/(2*mass_SplitQs[i_SplitQ]);

        vel_SplitQs[i_SplitQ] += \
        force_old*time_step/(2*mass_SplitQs[i_SplitQ]);

     }

     compute_atomic_Qs();

     for(i_SplitQ=0;i_SplitQ<n_splitQs;i_SplitQ++) {
        i_atom = atom_indices_SplitQs[i_SplitQ][0];
        j_atom = atom_indices_SplitQs[i_SplitQ][1];
        // Force calculation
        bond_force = force_bond_elastic(i_SplitQ);
        damping_force = force_Langevin(i_SplitQ);
        atomic_force = force_atomic_elastic(i_atom,j_atom);
        elect_force = force_electrostatics(i_atom,j_atom);
        force_new = bond_force + atomic_force + damping_force + \
        elect_force;
   
        // vel propagation
        vel_SplitQs[i_SplitQ] += \
        force_new*time_step/(2*mass_SplitQs[i_SplitQ]);
     }

}
//////////////////////////////////////////////////////////////////////
// Monitoring the split charges
void monitor_Qs(int i_step) {

     int i_atom;

     FILE *file_output_ener, *file_output;
     
     file_output = fopen("atomic_Qs.dat","a");
     file_output_ener = fopen("SQE_energy.dat","a");
     fprintf(file_output,"%d ",i_step);
     fprintf(file_output_ener,"%d %f\n",i_step,ecurrent);
     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        fprintf(file_output,"%f ",atom_Q[i_atom]);
     }
     fprintf(file_output,"\n");
     fclose(file_output);
     fclose(file_output_ener);

}
/////////////////////////////////////////////////////////////////////////
// Running MD on the split charges
void md_on_SplitQs(void) {

     int out, n_step_monitor, n_steps_collect, i_step, \
         counter, i_loop;
     double avrg_Q, *atom_Q_tmp, std_dv;

     out = system("rm atomic_Qs.dat SQE_energy.dat");

     n_step_monitor = 15;
     n_steps_collect = 100;
     avrg_Q = 0.0;
     
     atom_Q_tmp = create_1d_double_array(n_steps_collect,"");
     i_step = 1;
     counter = 1;
     label_top:
     predict_SplitQs();
     ecurrent = energy_total();
     if((i_step-1)%n_step_monitor == 0) monitor_Qs(i_step);
     atom_Q_tmp[counter-1] = atom_Q[0];
     avrg_Q += atom_Q[0]/n_steps_collect;
     counter++;
     if(i_step%n_steps_collect == 0) {
       std_dv = 0.0;
       for(i_loop=0;i_loop<n_steps_collect;i_loop++) {
          std_dv += pow((atom_Q_tmp[i_loop]/avrg_Q - 1),2);
       }
       std_dv = sqrt(std_dv/n_steps_collect);
       if(std_dv <= error_tol) {
        goto label_end;
       } else {
        counter = 1;
        avrg_Q = 0.0;
       }
     }
     i_step++;
     goto label_top;     
     label_end:;
     monitor_Qs(i_step);

     destroy_1d_double_array(atom_Q_tmp);
     

}
