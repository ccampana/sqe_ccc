#include "forces.h"
///////////////////////////////////////////////////////////////////////////
// Converting two indices into one for atomic array labeling
int get_index(int i_index, int j_index) {

    int new_index;

    new_index = i_index*n_atoms + j_index;

    return new_index;

}
////////////////////////////////////////////////////////////////////////////
// Computing damping force
double force_Langevin(int i_SplitQ) {

       double damping_force;

       damping_force = \
       -1.0*damping_factor*vel_SplitQs[i_SplitQ]*mass_SplitQs[i_SplitQ];

       return damping_force;

}
/////////////////////////////////////////////////////////////////////////////
// Computing elastic force from the bond hardness
double force_bond_elastic(int i_SplitQ) {

       double bond_force;

       bond_force = -1.0*(Kappa_SplitQs[i_SplitQ]*SplitQs[i_SplitQ] + \
       Psi_SplitQs[i_SplitQ])/multiplicity_SplitQs[i_SplitQ];

       return bond_force;

}
/////////////////////////////////////////////////////////////////////////////
// Computing elastic force from atomic contribution
double force_atomic_elastic(int i_atom, int j_atom) {

       int i_SplitQ;

       double atomic_force;

       atomic_force = -1.0*(atom_Kappa[i_atom]*atom_Q[i_atom] - \
       atom_Kappa[j_atom]*atom_Q[j_atom] + atom_Psi[i_atom] - \
       atom_Psi[j_atom]);

       return atomic_force;

}
//////////////////////////////////////////////////////////////////////////////
// Computing force from Ewald reciprocal term
double force_recp_term(int i_atom, int j_atom) {

       int k_atom, index_2D;
       double force_recp;

       index_2D = get_index(i_atom,j_atom);

       force_recp = 0.0;
       for(k_atom=0;k_atom<n_atoms;k_atom++) {
          force_recp -= atom_Q[k_atom]*recp_term_elect[k_atom][index_2D];
       }

       return force_recp;

}
//////////////////////////////////////////////////////////////////////////////
// Computing force from Ewald real term
double force_real_term(int i_atom, int j_atom) {

       int i, k_atom, l_atom;
       double force_real, prefact;

       force_real = 0.0;
       for(i=0;i<2;i++) {
          if(i == 0) {
            l_atom = i_atom;
            prefact = 1.0;
          } else {
            l_atom = j_atom;
            prefact = -1.0;
          }
          for(k_atom=0;k_atom<n_atoms;k_atom++) {
             force_real -= \
             prefact*atom_Q[k_atom]*real_term_elect[l_atom][k_atom];
          }
       }

       return force_real;

}
//////////////////////////////////////////////////////////////////////////////
// Computing force from Ewald self-term
double force_self_term(int i_atom, int j_atom) {

       double force_self;

       force_self = 2.0*sqrt(alpha_Ewald/PI)*(atom_Q[i_atom] - atom_Q[j_atom]);

       return force_self;

}
//////////////////////////////////////////////////////////////////////////////
// Computing total electrostatic force
double force_electrostatics(int i_atom, int j_atom) {

       double force_elect, force_real, force_recp, force_self;


       force_real = force_real_term(i_atom,j_atom);
       force_recp = force_recp_term(i_atom,j_atom);
       force_self = force_self_term(i_atom,j_atom);
       force_elect = force_real + force_recp + force_self;

       return force_elect;

}
