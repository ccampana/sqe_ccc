#include "init.h"
///////////////////////////////////////////////////////////////////////////////
void initialization(int Natoms, double** kappa_bond, int** bond_mult, \
     double** chi_bond, double** atom_coords, double* atom_radii) {

     int i_splitQ, i_atom, j_atom, i_index, i_dim;

     n_atoms = Natoms;
     i_splitQ = 0;
     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        for(j_atom=i_atom+1;j_atom<n_atoms;j_atom++) {
           if(bond_mult[i_atom][j_atom] != 0) {
             i_splitQ++;
           }
        }
     }
     n_splitQs = i_splitQ;

     // Allocating memory for arrays
     SplitQs = \
     create_1d_double_array(n_splitQs,"split charges");
     vel_SplitQs = \
     create_1d_double_array(n_splitQs,"split charge velocities");
     mass_SplitQs = \
     create_1d_double_array(n_splitQs,"split charge masses");
     Kappa_SplitQs = \
     create_1d_double_array(n_splitQs,"split charge bond hardnesses");
     Psi_SplitQs = \
     create_1d_double_array(n_splitQs,"split charge electronegativities");
     multiplicity_SplitQs = \
     create_1d_int_array(n_splitQs,"split charge multiplicity");
     atom_indices_SplitQs = create_2d_int_array(n_splitQs,2,"");
     atom_Q = \
     create_1d_double_array(n_atoms,"atomic charges");
     indices_SplitQs = \
     create_2d_int_array(n_atoms,n_atoms,"");
     real_term_elect = \
     create_2d_double_array(n_atoms,n_atoms,"");
     recp_term_elect = \
     create_2d_double_array(n_atoms,n_atoms*n_atoms,"");
     real_lat_vec = create_2d_double_array(NDIM,NDIM,"real lattice vectors");
     recp_lat_vec = create_2d_double_array(NDIM,NDIM,"reciprocal lattice vectors");
     atom_pos = create_2d_double_array(n_atoms,NDIM,"atomic positions");
     atom_rad = create_1d_double_array(n_atoms,"atomic radii");
     atom_Psi = create_1d_double_array(n_atoms,"atomic electronegativities");
     atom_Kappa = create_1d_double_array(n_atoms,"atomic hardnesses");
     sum_real =  create_2d_double_array(n_atoms,n_atoms,"");        
     atom_Q_anneal_ref = create_1d_double_array(n_atoms,"reference charges");

     // Arrays initialization
     i_splitQ = -1;
     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        atom_Psi[i_atom] = chi_bond[i_atom][i_atom]; 
        atom_Kappa[i_atom] = kappa_bond[i_atom][i_atom];
        atom_rad[i_atom] = atom_radii[i_atom];
        for(i_dim=0;i_dim<NDIM;i_dim++) {
           atom_pos[i_atom][i_dim] = atom_coords[i_atom][i_dim];
        }
        for(j_atom=i_atom+1;j_atom<n_atoms;j_atom++) {
           indices_SplitQs[i_atom][j_atom] = -1;
           indices_SplitQs[j_atom][i_atom] = -1;
           if(bond_mult[i_atom][j_atom] != 0) {
             i_splitQ++;
             vel_SplitQs[i_splitQ] = 0.0;
             SplitQs[i_splitQ] = 0.0;
             multiplicity_SplitQs[i_splitQ] = bond_mult[i_atom][j_atom];
             Kappa_SplitQs[i_splitQ] = kappa_bond[i_atom][j_atom];
             Psi_SplitQs[i_splitQ] = chi_bond[i_atom][j_atom];
             mass_SplitQs[i_splitQ] = 1.0;
             indices_SplitQs[i_atom][j_atom] = i_splitQ;
             indices_SplitQs[j_atom][i_atom] = i_splitQ;
             atom_indices_SplitQs[i_splitQ][0] = i_atom;
             atom_indices_SplitQs[i_splitQ][1] = j_atom;
           }
        }
     }

     // Parameter initialization
     alpha_Ewald = 0.01;
     charge_flag = 1;
     k_order_max = 7;
     k_order_2_max = k_order_max*k_order_max;
     n_order_max[0] = n_order_max[1] = n_order_max[2] = 2;
     damping_factor = 0.2;
     time_step = 0.04;
     error_tol = 0.000001;
     kvec = create_2d_double_array(pow(2*k_order_max+1,3),4,"");

}
/////////////////////////////////////////////////////////////////////////
// Computing matrix inverse
double matrix_inverse(double**Matrix,double**Matrix_inv) {

       double det;

       det = \
       Matrix[0][0]*(Matrix[2][2]*Matrix[1][1] - Matrix[2][1]*Matrix[1][2]) - \
       Matrix[1][0]*(Matrix[2][2]*Matrix[0][1] - Matrix[2][1]*Matrix[0][2]) + \
       Matrix[2][0]*(Matrix[1][2]*Matrix[0][1] - Matrix[1][1]*Matrix[0][2]);

       Matrix_inv[0][0] = \
       (Matrix[2][2]*Matrix[1][1] - Matrix[2][1]*Matrix[1][2])/det;
       Matrix_inv[0][1] = \
       -(Matrix[2][2]*Matrix[0][1] - Matrix[2][1]*Matrix[0][2])/det;
       Matrix_inv[0][2] = \
       (Matrix[1][2]*Matrix[0][1] - Matrix[1][1]*Matrix[0][2])/det;
       Matrix_inv[1][0] = \
       -(Matrix[2][2]*Matrix[1][0] - Matrix[2][0]*Matrix[1][2])/det;
       Matrix_inv[1][1] = \
       (Matrix[2][2]*Matrix[0][0] - Matrix[2][0]*Matrix[0][2])/det;
       Matrix_inv[1][2] = \
       -(Matrix[1][2]*Matrix[0][0] - Matrix[1][0]*Matrix[0][2])/det;
       Matrix_inv[2][0] = \
       (Matrix[2][1]*Matrix[1][0] - Matrix[2][0]*Matrix[1][1])/det;
       Matrix_inv[2][1] = \
       -(Matrix[2][1]*Matrix[0][0] - Matrix[2][0]*Matrix[0][1])/det;
       Matrix_inv[2][2] = \
       (Matrix[1][1]*Matrix[0][0] - Matrix[1][0]*Matrix[0][1])/det;

       return det;

}
///////////////////////////////////////////////////////////////////////
// Setting lattice vectors (real and reciprocal)
void set_lattice_vectors(double** lattice_vec) {

     int i_dim, j_dim;
     double **h_mat_inv, dist;     
     
     for(i_dim=0;i_dim<NDIM;i_dim++) {
        for(j_dim=0;j_dim<NDIM;j_dim++) {
           real_lat_vec[i_dim][j_dim] = lattice_vec[i_dim][j_dim];
        }
     }

     h_mat_inv = create_2d_double_array(NDIM,NDIM,"");
     Vol_cell = matrix_inverse(real_lat_vec,h_mat_inv);
     for(i_dim=0;i_dim<NDIM;i_dim++) {
        for(j_dim=0;j_dim<NDIM;j_dim++) { 
           recp_lat_vec[i_dim][j_dim] = 2.0*PI*h_mat_inv[i_dim][j_dim];
        }
     }  

    // For the real space calculation
    R_cutoff = PI/sqrt(alpha_Ewald);
    printf("Cutoff for real space sum is %f\n",R_cutoff);
    for(j_dim=0;j_dim<NDIM;j_dim++){
       dist = 0.0;
       for(i_dim=0;i_dim<NDIM;i_dim++){
          dist += pow(real_lat_vec[j_dim][i_dim],2);
       }
       dist = sqrt(dist);
       n_order_max[j_dim] = static_cast<int> (floor(R_cutoff/dist)+1);
       printf("# of cells in %d direction = %d\n",j_dim+1,n_order_max[j_dim]);
    }

}
////////////////////////////////////////////////////////////////////////
// Clearing memory
void clear_memory(void) {


     destroy_1d_double_array(SplitQs);
     destroy_1d_double_array(vel_SplitQs);
     destroy_1d_double_array(mass_SplitQs);
     destroy_1d_double_array(Kappa_SplitQs);
     destroy_1d_double_array(Psi_SplitQs);
     destroy_1d_int_array(multiplicity_SplitQs);
     destroy_2d_int_array(atom_indices_SplitQs);
     destroy_1d_double_array(atom_Q);
     destroy_2d_int_array(indices_SplitQs);
     destroy_2d_double_array(real_term_elect);
     destroy_2d_double_array(recp_term_elect);
     destroy_2d_double_array(real_lat_vec);
     destroy_2d_double_array(recp_lat_vec);
     destroy_2d_double_array(atom_pos);
     destroy_1d_double_array(atom_rad);
     destroy_1d_double_array(atom_Psi);
     destroy_1d_double_array(atom_Kappa);
     destroy_2d_double_array(sum_real);
     destroy_2d_double_array(kvec);
     destroy_1d_double_array(atom_Q_anneal_ref);

}
