#include "ewald.h"
using namespace std;
/////////////////////////////////////////////////////////////////////////////
// Computing reciprocal term for forces in the split charge calculation
void compute_recp_term(void) {

     int i_atom, j_atom, k_atom, index_2D, k_x, k_y, k_z;
     int k_order[NDIM], i_dim, j_dim, k_counter; 
     double k_order_2, V_pot_recp, k_dot_r_jk, k_dot_r_ik;
     double prefact, k_vec_loc[NDIM], exponent, k_vec_loc_2, \
            delta_r[NDIM], **cos_delta_kl;
/*  
     prefact = 4.0*PI/Vol_cell;
     for(k_atom=0;k_atom<n_atoms;k_atom++) {
        for(j_atom=0;j_atom<n_atoms;j_atom++) {
           for(i_atom=0;i_atom<n_atoms;i_atom++) {
              index_2D = get_index(j_atom,i_atom);
              recp_term_elect[k_atom][index_2D] = 0.0;
              for(k_z=-k_order_max;k_z<=k_order_max;k_z++) {
                 for(k_y=-k_order_max;k_y<=k_order_max;k_y++) {
                    for(k_x=-k_order_max;k_x<=k_order_max;k_x++) {
                       k_order[0] = k_x;
                       k_order[1] = k_y;
                       k_order[2] = k_z;
                       k_vec_loc_2 = 0.0;
                       k_order_2 = 0;
                       for(j_dim=0;j_dim<NDIM;j_dim++) {
                          k_vec_loc[j_dim] = 0.0;                          
                          for(i_dim=0;i_dim<NDIM;i_dim++) {
                             k_vec_loc[j_dim] += \
                             recp_lat_vec[i_dim][j_dim]*k_order[i_dim];
                          }
                          k_order_2 += pow(k_order[j_dim],2);
                          k_vec_loc_2 += pow(k_vec_loc[j_dim],2);
                       }
                       if((k_order_2 <= k_order_2_max) && (k_order_2 != 0)) {
                         exponent = k_vec_loc_2/(4.0*alpha_Ewald);
                         V_pot_recp = prefact*exp(-exponent)/k_vec_loc_2;
                         k_dot_r_jk = 0.0;
                         k_dot_r_ik = 0.0;
                         for(i_dim=0;i_dim<NDIM;i_dim++) {
                            delta_r[i_dim] = atom_pos[k_atom][i_dim] - \
                            atom_pos[j_atom][i_dim];
                            k_dot_r_jk += delta_r[i_dim]*k_vec_loc[i_dim];
                            delta_r[i_dim] = atom_pos[k_atom][i_dim] - \
                            atom_pos[i_atom][i_dim];
                            k_dot_r_ik += delta_r[i_dim]*k_vec_loc[i_dim];
                         }
                         recp_term_elect[k_atom][index_2D] += \
                         V_pot_recp*(cos(k_dot_r_jk)-cos(k_dot_r_ik));
                       }
                    }
                 }
              }
           }
        }
     }

*/
     // New and faster implementation of the reciprocal space
     // term calculation
     cos_delta_kl = create_2d_double_array(n_atoms,n_atoms,"");

     for(k_atom=0;k_atom<n_atoms;k_atom++) {
        for(j_atom=0;j_atom<n_atoms;j_atom++) {
           cos_delta_kl[k_atom][j_atom] = 0.0;
           for(k_counter=0;k_counter<ktot;k_counter++) {
              V_pot_recp = kvec[k_counter][0];
              k_dot_r_jk = 0.0;
              for(i_dim=0;i_dim<NDIM;i_dim++) {
                 k_dot_r_jk += \
                 (atom_pos[k_atom][i_dim] - \
                 atom_pos[j_atom][i_dim])*kvec[k_counter][i_dim+1];
              }
              cos_delta_kl[k_atom][j_atom] += \
              V_pot_recp*cos(k_dot_r_jk);
           }           
        }
     }

     for(k_atom=0;k_atom<n_atoms;k_atom++) {
        for(j_atom=0;j_atom<n_atoms;j_atom++) {
           for(i_atom=0;i_atom<n_atoms;i_atom++) {
              index_2D = get_index(j_atom,i_atom);
              recp_term_elect[k_atom][index_2D] = \
              (cos_delta_kl[k_atom][j_atom] - \
              cos_delta_kl[k_atom][i_atom]);
           }
        }
     }

     destroy_2d_double_array(cos_delta_kl);

}
/////////////////////////////////////////////////////////////////////////////
// Comnputing real term for forces in the split charge calculation
void compute_real_term(void) {

     int i_atom, j_atom, i_cell_x, i_cell_y, i_cell_z, i_dim;
     double delta_ij_2, delta_ij[NDIM], alpha_chosen;

     for(j_atom=0;j_atom<n_atoms;j_atom++) {
        for(i_atom=0;i_atom<n_atoms;i_atom++) {
           real_term_elect[j_atom][i_atom] = 0.0;
           for(i_cell_z=-n_order_max[2];i_cell_z<=n_order_max[2];i_cell_z++) {
              for(i_cell_y=-n_order_max[1];i_cell_y<=n_order_max[1];i_cell_y++) {
                 for(i_cell_x=-n_order_max[0];i_cell_x<=n_order_max[0];i_cell_x++) {
                    if((i_cell_x == 0) && (i_cell_y == 0) && (i_cell_z == 0)) {
                      if(i_atom == j_atom) goto skip_calc;
                    }
                    delta_ij_2 = 0.0;
                    for(i_dim=0;i_dim<NDIM;i_dim++) {
                       delta_ij[i_dim] = atom_pos[j_atom][i_dim] - \
                       (atom_pos[i_atom][i_dim] + \
                       i_cell_x*real_lat_vec[0][i_dim] + \
                       i_cell_y*real_lat_vec[1][i_dim] + \
                       i_cell_z*real_lat_vec[2][i_dim]);
                       delta_ij_2 += pow(delta_ij[i_dim],2);
                    }
                    delta_ij_2 = sqrt(delta_ij_2);
                    if(delta_ij_2 < R_cutoff) {
                      if(charge_flag == 0) {
                        real_term_elect[j_atom][i_atom] += \
                        erfc(sqrt(alpha_Ewald)*delta_ij_2)/delta_ij_2;
                      } else {
                        alpha_chosen = 1.0/sqrt((2*pow(atom_rad[i_atom],2) + \
                        2*pow(atom_rad[j_atom],2)));
                        real_term_elect[j_atom][i_atom] += \
                        erf(alpha_chosen*delta_ij_2)/delta_ij_2 - \
                        erf(sqrt(alpha_Ewald)*delta_ij_2)/delta_ij_2;
                      }                    
                      // Here Slater or similar corrections are needed
                      skip_calc:;                     
                    }
                 }
              }
           }
        }
     }

}
/////////////////////////////////////////////////////////////////////////
// Computing reciprocal term for energy in the split charge calculation
void compute_recp_term_ener(void) {

     int i_atom, k_x, k_y, k_z, k_counter;
     int k_order[NDIM], i_dim, j_dim;
     double k_order_2;
     double prefact, k_vec_loc[NDIM], exponent, k_vec_loc_2;

     k_counter = -1;
     prefact = 4.0*PI/Vol_cell;     
     for(k_z=-k_order_max;k_z<=k_order_max;k_z++) {
        for(k_y=-k_order_max;k_y<=k_order_max;k_y++) {
           for(k_x=-k_order_max;k_x<=k_order_max;k_x++) {
              k_order[0] = k_x;
              k_order[1] = k_y;
              k_order[2] = k_z;
              k_vec_loc_2 = 0.0;
              k_order_2 = 0;
              for(j_dim=0;j_dim<NDIM;j_dim++) {
                 k_vec_loc[j_dim] = 0.0;
                 for(i_dim=0;i_dim<NDIM;i_dim++) {
                    k_vec_loc[j_dim] += \
                    recp_lat_vec[i_dim][j_dim]*k_order[i_dim];
                 }
                 k_order_2 += pow(k_order[j_dim],2);
                 k_vec_loc_2 += pow(k_vec_loc[j_dim],2);
              }
              if((k_order_2 <= k_order_2_max) && (k_order_2 != 0)) {
                 k_counter++;
                 exponent = k_vec_loc_2/(4.0*alpha_Ewald);
                 kvec[k_counter][0] = prefact*exp(-exponent)/k_vec_loc_2;
                 for(i_dim=0;i_dim<NDIM;i_dim++) {
                     kvec[k_counter][i_dim+1] = k_vec_loc[i_dim];
                 }
              }
           }
        }
     }
     ktot = k_counter;

}
///////////////////////////////////////////////////////////////////////////
// Computing energy in reciprocal space
double compute_recp_ener(void) {

     int k_counter, i_atom, i_dim;
   
     double Ewald_sum_recp, k_dot_r;

     complex<double> rho_Q, V_pot_recp;

     Ewald_sum_recp = 0.0;
     for(k_counter=0;k_counter<ktot;k_counter++) {
        rho_Q = complex<double>(0.0,0.0);
        for(i_atom=0;i_atom<n_atoms;i_atom++){
           k_dot_r = 0.0;
           for(i_dim=0;i_dim<NDIM;i_dim++) {
               k_dot_r += atom_pos[i_atom][i_dim]*kvec[k_counter][i_dim+1];
           }
           rho_Q += \
           atom_Q[i_atom]*complex<double>(cos(k_dot_r),sin(k_dot_r));
        }
        V_pot_recp = \
        complex<double>(kvec[k_counter][0],0.0);
        V_pot_recp *= rho_Q*conj(rho_Q);
        Ewald_sum_recp += V_pot_recp.real();        
     }
     Ewald_sum_recp /= 2;
     return Ewald_sum_recp;

}
////////////////////////////////////////////////////////////////////
// Computing real term for energy in the split charge calculation
void compute_real_term_ener(void) {

     int i_atom, j_atom, i_cell_x, i_cell_y, i_cell_z, i_dim;
     double delta_ij_2, delta_ij[NDIM], alpha_chosen;

     for(i_atom=0;i_atom<n_atoms;i_atom++) {
        for(j_atom=0;j_atom<n_atoms;j_atom++) {
           sum_real[i_atom][j_atom] = 0.0;
           for(i_cell_z=-n_order_max[2];i_cell_z<=n_order_max[2];i_cell_z++) {
              for(i_cell_y=-n_order_max[1];i_cell_y<=n_order_max[1];i_cell_y++) {
                 for(i_cell_x=-n_order_max[0];i_cell_x<=n_order_max[0];i_cell_x++) {
                    if((i_cell_x == 0) && (i_cell_y == 0) && (i_cell_z == 0)) {
                      if(i_atom == j_atom) goto skip_calc;
                    }
                    delta_ij_2 = 0.0;
                    for(i_dim=0;i_dim<NDIM;i_dim++) {
                       delta_ij[i_dim] = atom_pos[i_atom][i_dim] - \
                       (atom_pos[j_atom][i_dim] + \
                       i_cell_x*real_lat_vec[0][i_dim] + \
                       i_cell_y*real_lat_vec[1][i_dim] + \
                       i_cell_z*real_lat_vec[2][i_dim]);
                       delta_ij_2 += pow(delta_ij[i_dim],2);
                    }
                    delta_ij_2 = sqrt(delta_ij_2);
                    if(delta_ij_2 < R_cutoff) {
                      if(charge_flag == 0) {
                        sum_real[i_atom][j_atom] += \
                        erfc(sqrt(alpha_Ewald)*delta_ij_2)/delta_ij_2;
                      } else {
                        alpha_chosen = 1.0/sqrt((2*pow(atom_rad[i_atom],2) + \
                        2*pow(atom_rad[j_atom],2)));
                        sum_real[i_atom][j_atom] += \
                        erf(alpha_chosen*delta_ij_2)/delta_ij_2 - \
                        erf(sqrt(alpha_Ewald)*delta_ij_2)/delta_ij_2;
                      }
                      // Here Slater or similar corrections are needed 
                      skip_calc:;
                    }
                 }
              }
           }          
        }
     }            

}
///////////////////////////////////////////////////////////////////////////
// Computing energy in real space
double compute_real_ener(void) {

       int i_atom, j_atom;
       double Ewald_sum_real;

       Ewald_sum_real = 0.0;
        for(i_atom=0;i_atom<n_atoms;i_atom++) {
          for(j_atom=0;j_atom<n_atoms;j_atom++) {
             Ewald_sum_real += \
             atom_Q[i_atom]*atom_Q[j_atom]*sum_real[i_atom][j_atom];
          }
        }
        Ewald_sum_real /= 2;
        return Ewald_sum_real;

}
///////////////////////////////////////////////////////////////////////////
// Computing self term for energy in the split charge calculation
double compute_self_term_ener(void) {

       int i_atom;
       double Ewald_sum_self;

       Ewald_sum_self = 0.0;
       for(i_atom=0;i_atom<n_atoms;i_atom++) {
          Ewald_sum_self -= pow(atom_Q[i_atom],2)*sqrt(alpha_Ewald/PI);
       }
       return Ewald_sum_self;

}
/////////////////////////////////////////////////////////////////////////////

