#include "vars_arrays.h"
/////////////////////////////////////////////////////////////////////////
// SplitQs variables
int n_splitQs, n_atoms, k_order_max, k_order_2_max, n_order_max[NDIM];

int *multiplicity_SplitQs, **atom_indices_SplitQs, **indices_SplitQs, \
    charge_flag;

double R_cutoff;

double damping_factor, error_tol, time_step, Vol_cell, alpha_Ewald;

double *SplitQs, *vel_SplitQs, *mass_SplitQs, *Kappa_SplitQs, **atom_pos, \
        *atom_Psi, *atom_Kappa, *atom_rad, *Psi_SplitQs;

double *atom_Q, **recp_term_elect, **real_term_elect, **real_lat_vec, \
       **recp_lat_vec;

int    ktot;

double **sum_real, **sum_recp, **kvec;

////////////////////////////////////////////////////////////////////////
// CG-variables
double ecurrent, *hcg, *fcg, *x0cg, *xcg;

////////////////////////////////////////////////////////////////////////
// Simulated annealing variables
int n_anneal_moves, n_params;

double Temp_max, *anneal_params;

double *atom_Q_anneal_ref;
